package smart_exp_java.models;

import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import smart_exp_java.enums.Baudrate;
import smart_exp_java.enums.Databits;
import smart_exp_java.enums.Parity;
import smart_exp_java.enums.Stopbits;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class SerialMonitor implements SerialPortEventListener {

    private final SerialPort serialPort;
    private final BlockingQueue<String> queue;
    private StringBuffer currentMessage = new StringBuffer();

    public SerialMonitor(final String portName, final Baudrate baudrate, final Databits databits, final Stopbits stopbits, final Parity parity) throws SerialPortException {
        this.queue = new ArrayBlockingQueue<>(100);

        this.serialPort = new SerialPort(portName);
        serialPort.openPort();
        serialPort.setParams(baudrate.getRate(), databits.getBits(), stopbits.getStopbits(), parity.getParity());
        serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_RTSCTS_IN | SerialPort.FLOWCONTROL_RTSCTS_OUT);
        serialPort.addEventListener(this);
    }

    @Override
    public void serialEvent(SerialPortEvent serialPortEvent) {
        try {
            if (serialPortEvent.isRXCHAR()) {
                String message = serialPort.readString(serialPortEvent.getEventValue());
                message = message.replaceAll("\r", "");
                currentMessage.append(message);

                boolean goAhead = true;
                while (goAhead) {
                    message = currentMessage.toString();
                    int index = message.indexOf("\n");
                    if (index >= 0) {
                        queue.put(message.substring(0, index));
                        currentMessage = new StringBuffer("");
                        if (index + 1 < message.length()) {
                            currentMessage.append(message.substring(index + 1));
                        }
                    } else {
                        goAhead = false;
                    }
                }
            }
        } catch (Exception exception) {
            System.out.println("Error in reading string from " + serialPort.getPortName() + ":" + exception.getLocalizedMessage());
        }
    }

    public void sendMessage(String message) throws SerialPortException {
        message = message + "\n";
        synchronized (serialPort) {
            serialPort.writeBytes(message.getBytes());
        }
    }

    public boolean isMessageAvailable() {
        return !queue.isEmpty();
    }

    public String receiveMessage() throws InterruptedException {
        return queue.take();
    }

    public void close() throws SerialPortException {
        serialPort.removeEventListener();
        serialPort.closePort();
    }

}
