package smart_exp_java.tasks;

import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import smart_exp_java.controllers.MainController;
import smart_exp_java.models.SerialMonitor;

public class SerialReader extends ScheduledService<Void> {

    private final MainController controller;
    private final SerialMonitor serialMonitor;

    public SerialReader(MainController controller, SerialMonitor serialMonitor) {
        this.controller = controller;
        this.serialMonitor = serialMonitor;
    }

    @Override
    protected Task<Void> createTask() {
        return new Task<>() {
            @Override
            protected Void call() throws Exception {
                if (serialMonitor.isMessageAvailable()) {
                    String message;
                    message = serialMonitor.receiveMessage();
                    controller.logData(message);
                }
                return null;
            }
        };
    }
}
