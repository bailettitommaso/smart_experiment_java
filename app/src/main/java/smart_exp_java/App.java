package smart_exp_java;

import javafx.application.Application;

public final class App {
    public static void main(final String[] args) {
        Application.launch(Launcher.class);
    }
}
