package smart_exp_java.enums;

import jssc.SerialPort;

public enum Databits {
    DATABITS_5(SerialPort.DATABITS_5),
    DATABITS_6(SerialPort.DATABITS_6),
    DATABITS_7(SerialPort.DATABITS_7),
    DATABITS_8(SerialPort.DATABITS_8);

    private final int bits;

    Databits(int bits) {
        this.bits = bits;
    }

    public int getBits() {
        return bits;
    }
}
