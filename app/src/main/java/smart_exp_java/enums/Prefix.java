package smart_exp_java.enums;

public enum Prefix {
	DATA("da:"),
	STATUS("st:"),
	SAMPLING_RATE("hz:");
	
	private final String prefix;
	
	Prefix(final String prefix){
		this.prefix = prefix;
	}
	
	public String getPrefix() {
		return this.prefix;
	}
}
