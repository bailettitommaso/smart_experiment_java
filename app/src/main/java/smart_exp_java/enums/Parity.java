package smart_exp_java.enums;

import jssc.SerialPort;

public enum Parity {
    PARITY_NONE(SerialPort.PARITY_NONE),
    PARITY_ODD(SerialPort.PARITY_ODD),
    PARITY_EVEN(SerialPort.PARITY_EVEN),
    PARITY_MARK(SerialPort.PARITY_MARK),
    PARITY_SPACE(SerialPort.PARITY_SPACE);

    private final int parity;

    Parity(int parity) {
        this.parity = parity;
    }

    public int getParity() {
        return parity;
    }
}
