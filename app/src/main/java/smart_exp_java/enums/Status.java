package smart_exp_java.enums;

public enum Status {
	SLEEP("sl","Sleeping"),
	READY("re","Ready"),
	MEASURE("me","Measuring"),
	ERROR("er","ERROR"),
	RESTART("rs","Restart");
	
	private final String legend;
	private final String state;
	
	Status(final String legend, final String state) {
		this.legend = legend;
		this.state = state;
	}
	
	public String getStatus() {
		return this.state;
	}

	public String getLegend() {
		return this.legend;
	}
}
