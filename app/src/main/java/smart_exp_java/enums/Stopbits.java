package smart_exp_java.enums;

import jssc.SerialPort;

public enum Stopbits {
    STOPBITS_1(SerialPort.STOPBITS_1),
    STOPBITS_1_5(SerialPort.STOPBITS_1_5),
    STOPBITS_2(SerialPort.STOPBITS_2);

    private final int stopbits;

    Stopbits(final int stopbits) {
        this.stopbits = stopbits;
    }

    public int getStopbits() {
        return stopbits;
    }
}
