package smart_exp_java.controllers;

import javafx.concurrent.ScheduledService;
import javafx.util.Duration;
import jssc.SerialPortException;
import smart_exp_java.enums.Prefix;
import smart_exp_java.enums.Status;
import smart_exp_java.models.SerialMonitor;
import smart_exp_java.tasks.SerialReader;
import smart_exp_java.views.MainView;

import java.util.Date;



public class MainController {

    private final MainView view;
    private final SerialMonitor serialMonitor;
    private final ScheduledService<Void> logger;
    private String lastStatus;
	private int measureNumber;
	private int lastVal;
	private float lastPos;
	private float lastSpeed;
	private long time;

    public MainController(final SerialMonitor serialMonitor) {
        this.serialMonitor = serialMonitor;
        this.view = new MainView(this);
        this.logger = new SerialReader(this, this.serialMonitor);
        this.logger.setPeriod(Duration.millis(15));
        this.logger.start();
        this.lastStatus = "";
    }

    public void logData(final String entry) {
        this.understandMessage(entry);
    }

    public void sendRestart() {
    	try {
			this.serialMonitor.sendMessage("next");
		} catch (SerialPortException e) {
			e.printStackTrace();
		}
    }
    
    public void exit() {
        this.logger.cancel();
        try {
            this.serialMonitor.close();
        } catch (SerialPortException e) {
            this.view.showError(e.getLocalizedMessage());
        }
    }
    
    private void understandMessage(final String message) {
    	if (message.contains(":")) {
    		final String prefix = message.substring(0, 3);
    		final String msg = message.substring(3);
    		
    		if (prefix.equals(Prefix.SAMPLING_RATE.getPrefix())) {
    			final int val = Integer.parseInt(msg);
    			if (val > 0 && val < 51 && val != this.lastVal) {
    				this.view.setFreq(Integer.toString(val));
    				this.lastVal = val;
    				this.time = new Date().getTime();
    			}
			    
			} else if (prefix.equals(Prefix.STATUS.getPrefix())) {
				if (this.isFirstTimeStatusChanged(msg)) {
					this.setStatus(msg);
					
				}
				this.lastStatus = msg;
			} else if (prefix.equals(Prefix.DATA.getPrefix())) {
				int distanceMilli = (int)Float.parseFloat(msg);
				distanceMilli = distanceMilli > 901 ? 900 : distanceMilli;
				long currentTime = new Date().getTime();
				int timePassed = (int) (currentTime - this.time);
				float currentSpeed = ((float)this.lastPos*1000 - ((float)distanceMilli))/timePassed;
				currentSpeed = currentSpeed > 2.01 ? 2 : currentSpeed < -2.01 ? -2 : currentSpeed >= 0.005 ? currentSpeed : currentSpeed <= -0.005 ? currentSpeed : 0;
				float acceleration = 1000*(currentSpeed - this.lastSpeed)/timePassed;
				acceleration = acceleration > 3.01 ? 3 : acceleration < -3.01 ? -3 : acceleration >= 0.005 ? acceleration : acceleration <= -0.005 ? acceleration : 0;
				double centimeters = ((double)distanceMilli)/10;
				this.view.addDistanceData("" + this.measureNumber, centimeters);
				this.view.addSpeedData("" + this.measureNumber, currentSpeed);
				this.view.addAccelerationData("" + this.measureNumber, acceleration);
				this.measureNumber++;
				this.lastSpeed = currentSpeed;
				this.lastPos = ((float)(distanceMilli))/1000;
				this.time = currentTime;
			}
		} else {
		}
    }

	private void setStatus(final String value) {
		if (value.equals(Status.SLEEP.getLegend())) {
			this.view.setStatus(Status.SLEEP);
			
		} else if (value.equals(Status.READY.getLegend())) {
			this.view.setStatus(Status.READY);
			
		} else if (value.equals(Status.MEASURE.getLegend())) {
			this.view.setStatus(Status.MEASURE);
			
		} else if (value.equals(Status.ERROR.getLegend())) {
			this.view.setStatus(Status.ERROR);
			this.view.errorMode();
		} else if (value.equals(Status.RESTART.getLegend())) {
			this.view.showRestartDialog();
		}
	}
	
	private boolean isFirstTimeStatusChanged(final String status) {
		return !status.equals(this.lastStatus);
	}
}
