package smart_exp_java.controllers;

import com.google.common.collect.ImmutableList;
import jssc.SerialPortException;
import jssc.SerialPortList;
import smart_exp_java.models.SerialMonitor;
import smart_exp_java.views.SetupView;

public class SetupController {

    private final SetupView view;

    public SetupController() {
        this.view = new SetupView(this, ImmutableList.copyOf(SerialPortList.getPortNames()));
    }

    public void connect() {
        try {
            new MainController(new SerialMonitor(this.view.getSerial(), this.view.getBaudrate(), this.view.getDatabits(), this.view.getStopbits(), this.view.getParity()));
            this.view.close();
        } catch (SerialPortException e) {
            this.view.showError(e.getLocalizedMessage());
        }
    }
}
