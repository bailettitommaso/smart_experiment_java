package smart_exp_java;

import javafx.application.Application;
import javafx.stage.Stage;
import smart_exp_java.controllers.SetupController;

public class Launcher extends Application {
    @Override
    public void start(final Stage primaryStage) {
        new SetupController();
    }
}
