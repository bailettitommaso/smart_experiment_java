package smart_exp_java.views;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

public abstract class View {

    private final Stage stage;

    public View(final URL url) {
        this.stage = new Stage();
        final FXMLLoader loader = new FXMLLoader(url);
        loader.setController(this);
        try {
            this.stage.setScene(new Scene(loader.load()));
        } catch (IOException e) {
            new Alert(Alert.AlertType.ERROR, e.getLocalizedMessage()).showAndWait();
            Platform.exit();
        }
    }

    public Stage getStage() {
        return stage;
    }

    public void show() {
        this.stage.show();
    }

    public void showError(final String message) {
        new Alert(Alert.AlertType.ERROR, message).showAndWait();
    }

    public void close() {
        this.stage.close();
    }
}
