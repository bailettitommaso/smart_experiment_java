package smart_exp_java.views;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import smart_exp_java.controllers.MainController;
import smart_exp_java.enums.Status;

public class MainView extends View {
	@FXML
    private ScrollPane accelerationScrollPane;

    @FXML
    private CategoryAxis timeAccelerationAxis;

    @FXML
    private Label freqTitle;

    @FXML
    private ScrollPane speedScrollPane;

    @FXML
    private Label statusTitle;

    @FXML
    private ScrollPane distanceScrollPane;

    @FXML
    private LineChart<String, Double> distanceChart;

    @FXML
    private TextField freq;

    @FXML
    private LineChart<String, Double> speedChart;

    @FXML
    private LineChart<String, Double> accelerationChart;

    @FXML
    private CategoryAxis timeDistanceAxis;

    @FXML
    private NumberAxis accelerationAxis;

    @FXML
    private CategoryAxis speedTimeAxis;

    @FXML
    private NumberAxis speedAxis;

    @FXML
    private NumberAxis distanceAxis;

    @FXML
    private GridPane gridPane;

    @FXML
    private Label hzSignature;

    @FXML
    private Label status;

	private final MainController controller;
	private XYChart.Series<String, Double> surveyDistance;
	private XYChart.Series<String, Double> surveySpeed;
	private XYChart.Series<String, Double> surveyAcceleration;

	public MainView(final MainController controller) {
		super(ClassLoader.getSystemResource("views/serialViewNew.fxml"));
		this.controller = controller;
		this.getStage().setOnCloseRequest(event -> this.controller.exit());
		this.show();
		
		this.surveyDistance = new XYChart.Series<String, Double>();
		
		this.surveySpeed = new XYChart.Series<String, Double>();
		
		this.surveyAcceleration= new XYChart.Series<String, Double>();
		
		this.setChart();
		
		this.distanceChart.setCreateSymbols(false);
		this.speedChart.setCreateSymbols(false);
		this.accelerationChart.setCreateSymbols(false);
		this.distanceAxis.setUpperBound(91);
		
		this.distanceChart.setLegendVisible(false);
		this.speedChart.setLegendVisible(false);
		this.accelerationChart.setLegendVisible(false);

		this.distanceScrollPane.widthProperty().addListener((obs, oldVal, newVal) -> {
			this.resizeChart(this.distanceScrollPane.getHeight(), newVal.doubleValue());
		});

		this.distanceScrollPane.heightProperty().addListener((obs, oldVal, newVal) -> {
			this.resizeChart(newVal.doubleValue(), this.distanceScrollPane.getWidth());
		});
	}

	private void setChart() {
		this.distanceChart.getData().clear();
		this.distanceChart.getData().add(this.surveyDistance);
		this.speedChart.getData().clear();
		this.speedChart.getData().add(this.surveySpeed);
		this.accelerationChart.getData().clear();
		this.accelerationChart.getData().add(this.surveyAcceleration);
	}


	public void setFreq(final String string) {
		Platform.runLater(() -> {
			this.freq.setText(string);
		});
	}

	public void setStatus(final Status status) {
		Platform.runLater(() -> {
			this.status.setText(status.getStatus());
			this.status.setMinWidth(Region.USE_PREF_SIZE);
			Platform.runLater(() -> {
				this.status.setTextFill(Color.web("#000000", 1));
			});
		});
	}

	public void addDistanceData(String measureNumber, double distance) {
		Platform.runLater(() -> {
			this.surveyDistance.getData().add(new XYChart.Data<String, Double>(measureNumber, distance));
			if (this.surveyDistance.getData().size() % 30 == 0) {
				this.distanceChart.setMinSize(this.distanceChart.getWidth() + 250, this.distanceChart.getHeight());
			}
			this.distanceScrollPane.setHvalue(1);
		});
	}
	
	public void addSpeedData(String measureNumber, double speed) {
		Platform.runLater(() -> {
			this.surveySpeed.getData().add(new XYChart.Data<String, Double>(measureNumber, speed));
			if (this.surveySpeed.getData().size() % 30 == 0) {
				this.speedChart.setMinSize(this.speedChart.getWidth() + 250, this.speedChart.getHeight());
			}
			this.speedScrollPane.setHvalue(1);
		});
	}
	
	public void addAccelerationData(String measureNumber, double acceleration) {
		Platform.runLater(() -> {
			this.surveyAcceleration.getData().add(new XYChart.Data<String, Double>(measureNumber, acceleration));
			if (this.surveyAcceleration.getData().size() % 30 == 0) {
				this.accelerationChart.setMinSize(this.accelerationChart.getWidth() + 250, this.accelerationChart.getHeight());
			}
			this.accelerationScrollPane.setHvalue(1);
		});
	}
	

	public void errorMode() {
		Platform.runLater(() -> {
			this.status.setTextFill(Color.web("#ff0000", 1));
			this.showErrorDistance();
		});
	}

	public void showRestartDialog() {
		Platform.runLater(() -> {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Restart");
			alert.setHeaderText("Click ok to be ready to do another experiment!");
			alert.setContentText("");

			alert.showAndWait();
			this.resetChart();
			this.controller.sendRestart();
		});
	}

	public void resizeChart(double height, double width) {
		this.distanceChart.setPrefWidth(width);
		this.distanceChart.setPrefHeight(height);

		this.accelerationChart.setPrefWidth(width);
		this.accelerationChart.setPrefHeight(height);

		this.speedChart.setPrefWidth(width);
		this.speedChart.setPrefHeight(height);
	}

	private void showErrorDistance() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error");
		alert.setHeaderText("Object too distant!");
		alert.setContentText(
				"The object is too distant from sensor. The limit is 90 centimeters, move the object closer.");

		alert.show();
	}
	
	private void resetChart() {
		this.surveyDistance = new XYChart.Series<String, Double>();
		this.surveyDistance.setName("Distance");
		
		this.surveySpeed = new XYChart.Series<String, Double>();
		this.surveySpeed.setName("Speed");
		
		this.surveyAcceleration = new XYChart.Series<String, Double>();
		this.surveyAcceleration.setName("Acceleration");
		
		this.distanceChart.getData().clear();
		this.distanceChart.getData().add(this.surveyDistance);
		
		this.speedChart.getData().clear();
		this.speedChart.getData().add(this.surveySpeed);
		
		this.accelerationChart.getData().clear();
		this.accelerationChart.getData().add(this.surveyAcceleration);
	}
}
