package smart_exp_java.views;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import smart_exp_java.controllers.SetupController;
import smart_exp_java.enums.Baudrate;
import smart_exp_java.enums.Databits;
import smart_exp_java.enums.Parity;
import smart_exp_java.enums.Stopbits;

import java.util.Arrays;
import java.util.List;

public class SetupView extends View {

    @FXML
    private ChoiceBox<String> serialChoiceBox;
    @FXML
    private ChoiceBox<Baudrate> baudrateChoiceBox;
    @FXML
    private ChoiceBox<Databits> databitsChoiceBox;
    @FXML
    private ChoiceBox<Stopbits> stopbitsChoiceBox;
    @FXML
    private ChoiceBox<Parity> parityChoiceBox;
    @FXML
    private Button buttonConnect;

    private final SetupController controller;

    public SetupView(final SetupController controller, final List<String> availablePorts) {
        super(ClassLoader.getSystemResource("views/mainView.fxml"));
        this.controller = controller;

        this.serialChoiceBox.setItems(FXCollections.observableArrayList(availablePorts));
        if (!this.serialChoiceBox.getItems().isEmpty()) {
            this.serialChoiceBox.setValue(availablePorts.get(0));
        }
        this.baudrateChoiceBox.setItems(FXCollections.observableArrayList(Arrays.asList(Baudrate.values())));
        this.baudrateChoiceBox.setValue(Baudrate.BAUDRATE_115200);
        this.databitsChoiceBox.setItems(FXCollections.observableArrayList(Arrays.asList(Databits.values())));
        this.databitsChoiceBox.setValue(Databits.DATABITS_8);
        this.stopbitsChoiceBox.setItems(FXCollections.observableArrayList(Arrays.asList(Stopbits.values())));
        this.stopbitsChoiceBox.setValue(Stopbits.STOPBITS_1);
        this.parityChoiceBox.setItems(FXCollections.observableArrayList(Arrays.asList(Parity.values())));
        this.parityChoiceBox.setValue(Parity.PARITY_NONE);

        this.show();
    }

    @FXML
    private void clickConnect() {
        this.controller.connect();
    }

    public String getSerial() {
        return serialChoiceBox.getValue();
    }

    public Baudrate getBaudrate() {
        return baudrateChoiceBox.getValue();
    }

    public Databits getDatabits() {
        return databitsChoiceBox.getValue();
    }

    public Stopbits getStopbits() {
        return stopbitsChoiceBox.getValue();
    }

    public Parity getParity() {
        return parityChoiceBox.getValue();
    }
}
