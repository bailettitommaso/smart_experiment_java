plugins {
    // Apply the application plugin to add support for building a CLI application in Java.
    application
}

repositories {
    // Use JCenter for resolving dependencies.
    jcenter()
}

val javaFXModules = listOf("base", "controls", "fxml", "swing", "graphics")
val supportedPlatforms = listOf("linux", "mac", "win")

dependencies {
    // Use JUnit test framework.
    testImplementation("junit:junit:4.13")

    // This dependency is used by the application.
    implementation("com.google.guava:guava:29.0-jre")
    implementation("io.github.java-native:jssc:2.9.2")
    for(platform in supportedPlatforms) {
        for(module in javaFXModules) {
            implementation("org.openjfx:javafx-$module:15:$platform")
        }
    }
}

application {
    // Define the main class for the application.
    mainClass.set("smart_exp_java.App")
}
